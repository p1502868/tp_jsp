import fr.univlyon1.mif03.tpspring.beans.GestionMessage;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/Filter")
public class Filter implements javax.servlet.Filter {

    private GestionMessage gestionMessage = new GestionMessage();

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        HttpSession session = req.getSession();

        String uri = req.getRequestURI();

        /*if (uri.startsWith("/index.html") || uri.startsWith("/user") || uri.startsWith("/userCreation") || uri.startsWith("/back-office")) {
            if (req.getMethod().equalsIgnoreCase("POST")) {
                if (!req.getParameter("pseudo").isEmpty()) {
                    String pseudo = req.getParameter("pseudo");
                    session.setAttribute("pseudo", pseudo);
                    String salon = req.getParameter("salon");
                    session.setAttribute("salon", salon);
                    for(int i = 0; i < gestionMessage.getUserList().size(); i++) {
                        if (gestionMessage.getUserList().get(i).getPseudo().equals(session.getAttribute("pseudo"))) {
                            session.setAttribute("idUser",gestionMessage.getUserList().get(i).getPseudo());
                            res.sendRedirect("interface.html");
                            chain.doFilter(request, response);
                        }
                    }
                }
            }
            chain.doFilter(request, response);
        } else if ((!uri.startsWith("/index.html")) && session.getAttribute("pseudo") == null) {
            res.sendRedirect("index.html");

        } else {
            chain.doFilter(request, response);
        }*/
        chain.doFilter(request, response);
    }
    public void destroy() {
        //close any resources here
    }

}
