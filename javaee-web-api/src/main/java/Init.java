import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/Init")
public class Init extends HttpServlet  {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        response.sendRedirect("index.html");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        HttpSession session = request.getSession();

        if (!request.getParameter("pseudo").isEmpty()){
            if(!request.getParameter("salon").isEmpty()){
                String pseudo = request.getParameter("pseudo");
                session.setAttribute("pseudo",pseudo);
                String salon = request.getParameter("salon");
                session.setAttribute("salon",salon);
                this.getServletContext().getRequestDispatcher( "/interface.html" ).forward( request, response );
            }
        }

        response.sendRedirect("index.html");

    }


}
