package fr.univlyon1.mif03.tpspring.restjersey;

import fr.univlyon1.mif03.tpspring.beans.GestionMessage;
import fr.univlyon1.mif03.tpspring.model.Message;
import fr.univlyon1.mif03.tpspring.model.User;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@EnableWebMvc
@Path("/MessageService")
public class MessageService {

    GestionMessage message = new GestionMessage();

    @RequestMapping(value="changeMessageS/{i}/{j}/{k}", method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void changeMessageS(@PathVariable("i") String c, @PathVariable("j") int d, @PathVariable("k") String e) {
        message.getListMessage(c).get(d).setTexte(e);
    }

    @RequestMapping(value="changeMessage/{i}/{j}")
    public ModelAndView changeMessage(@PathVariable("i") String c, @PathVariable("j") String d) {
        String result = "";
        if (message.getListMessage(c).size() >=1) {
            message.getListMessage(c).get(message.getListMessage(c).size() - 1).setTexte(d);
            result = " d ";

        } else {
            result = "Une erreur est survenue";

        }
        Map<String, Object> map = new HashMap<>();
        map.put("message", result);
        return new ModelAndView("SimpleString", map);
    }

    @RequestMapping(value="changeMessage/{i}/{j}" ,
            produces={"application/xml", "application/json"},
            method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    List<Message> changeMessageXMLJSON(@PathVariable("i") String c,@PathVariable("j") String d) {
        if (message.getListMessage(c).size() >=1) {
            message.getListMessage(c).get(message.getListMessage(c).size() - 1).setTexte(d);
            return message.getListMessage(c);

        } else {
            return null;
        }
    }

    @RequestMapping(value="deleteMessageS/{i}/{j}", method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void deleteMessageS(@PathVariable("i") String c, @PathVariable("j") int d) {
        message.getListMessage(c).get(d).setTexte(null);
        message.getListMessage(c).get(d).setPseudo(null);
    }

    @RequestMapping(value="deleteMessage/{i}")
    public ModelAndView deleteMessage(@PathVariable("i") String c) {
        String result = "";
        if (message.getListMessage(c).size() >=1) {
            message.getListMessage(c).remove(message.getListMessage(c).size() - 1);
            result = "Message supprimé";

        } else {
            result = "Une erreur est survenue";
        }
        Map<String, Object> map = new HashMap<>();
        map.put("message", result);
        return new ModelAndView("SimpleString", map);
    }

    @RequestMapping(value="deleteMessage/{i}" ,
            produces={"application/xml", "application/json"},
            method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    List<Message> deleteMessageXMLJSON(@PathVariable("i") String c,@PathVariable("j") int d) {
        if (message.getListMessage(c).size() >=1) {
            message.getListMessage(c).remove(message.getListMessage(c).size() - 1);
            return message.getListMessage(c);

        } else {
            return null;
        }
    }

    @RequestMapping(value="messageInfo/{i}/{j}")
    public ModelAndView messageInfo(@PathVariable("i") String c,@PathVariable("j") int d) {
        String result = "";
        if (d>=0 && d < message.getListMessage(c).size()) {
            result = message.getListMessage(c).get(d).getPseudo() + " : " + message.getListMessage(c).get(d).getTexte();
        } else {
            result = "Une erreur est survenue";
        }
        Map<String, Object> map = new HashMap<>();
        map.put("message", result);
        return new ModelAndView("SimpleString", map);
    }

    @RequestMapping(value="messageInfo/{i}/{j}" ,
            produces={"application/xml", "application/json"},
            method= RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    Message messageInfoXMLJSON(@PathVariable("i") String c,@PathVariable("j") int d) {
        if (d>=0 && d < message.getListMessage(c).size()) {
            return message.getListMessage(c).get(d);
        } else {
            return null;
        }
    }
}
