package fr.univlyon1.mif03.tpspring.controller;

import fr.univlyon1.mif03.tpspring.beans.GestionMessage;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/fr.univlyon1.mif03.tpspring.controller.ControllerMessage")
public class ControllerMessage extends HttpServlet {

    private GestionMessage messages = new GestionMessage();

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        messages.setSalon(request.getSession().getAttribute("salon").toString());

        long lastModifiedFromBrowser = request.getDateHeader("If-Modified-Since");

        if (lastModifiedFromBrowser != -1
                || messages.getDerniereModif().getTime() <= lastModifiedFromBrowser
                || String.valueOf(messages.nbMessageSalon()).equals(request.getCookies()[0].getName())) {
            response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
        } else {
            response.addDateHeader("Last-Modified", messages.getDerniereModif().getTime());
        }

        this.getServletContext().getRequestDispatcher("/WEB-INF/Views/Messages.jsp").forward(request, response);

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher("/WEB-INF/Views/Stockage.jsp").forward( request, response );
    }

}

