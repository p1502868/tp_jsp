package fr.univlyon1.mif03.tpspring.beans;


import fr.univlyon1.mif03.tpspring.model.Message;
import fr.univlyon1.mif03.tpspring.model.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class GestionMessage {

    static HashMap<String, ArrayList<Message>> listMessage = new HashMap<>();
    static Date derniereModif = new Date();
    static ArrayList<String> ListSalon = new ArrayList<>();
    String salon;
    static ArrayList<User> userList = new ArrayList<User>();

    public GestionMessage(){ }

    public void setSalon(String salon){
        if (listMessage.get(salon) == null) {
            listMessage.put(salon,new ArrayList<Message>());
            ListSalon.add(salon);
        }
        this.salon = salon;
    }

    public ArrayList<Message> getListMessage(String salon){
        return listMessage.get(salon);
    }

    public void addMessage(String salon, Message message){
        listMessage.get(salon).add(message);
    }

    public void addMessage(Message message){
        listMessage.get(this.salon).add(message);
    }


    public int nbMessageSalon(String salon){
        return listMessage.get(salon).size();
    }

    public int nbMessageSalon(){
        return listMessage.get(this.salon).size();
    }


    public void setDerniereModif(Date d) {
        derniereModif = d;
    }

    public Date getDerniereModif() {
        return derniereModif;
    }

    public void addUser(User user) { userList.add(user);}

    public void delUser(String user) {
        for (int i =0; i < userList.size(); i++) {
            if(userList.get(i).getPseudo().equals(user)) userList.remove(i);
        }
    }

    public ArrayList<User> getUserList() {
        return userList;
    }

    public ArrayList<String> getListSalon() { return ListSalon;
    }
}

