package fr.univlyon1.mif03.tpspring.model;

public class Message {
    private String pseudo;
    private String texte;
    public int idMessage;

    public Message(String pseudo, String texte, int idMessage) {
        this.pseudo = pseudo;
        this.texte = texte;
        this.idMessage = idMessage;
    }

    public String getPseudo() {
        return pseudo;
    }

    public String getTexte() {
        return texte;
    }

    public void setTexte(String texte) {
        this.texte = texte;
    }

    public void setPseudo(String texte) {
        this.pseudo = texte;
    }
}
