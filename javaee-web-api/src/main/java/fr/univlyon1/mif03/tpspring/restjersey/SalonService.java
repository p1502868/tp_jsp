package fr.univlyon1.mif03.tpspring.restjersey;

import fr.univlyon1.mif03.tpspring.beans.GestionMessage;
import fr.univlyon1.mif03.tpspring.model.Message;
import fr.univlyon1.mif03.tpspring.model.User;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//https://crunchify.com/how-to-build-restful-service-with-java-using-jax-rs-and-jersey/
@Controller
@EnableWebMvc
@Path("/SalonService")
public class SalonService {
    GestionMessage message = new GestionMessage();

    /**
     *
     * @return la liste des salons
     */
    @RequestMapping(value = "getSalon" ,
            produces = {"application/xml", "application/json"},
            method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    List<String> getSalon() {
        return message.getListSalon();
    }

    /**
     *
     * @return ajoute un salon
     */
    @RequestMapping(value = "addSalon/{i}" ,
            method = RequestMethod.POST)
    public @ResponseBody
    void addSalon(@PathVariable("i") String c) {
        message.setSalon(c);
    }

    @RequestMapping(value="{i}")
    public ModelAndView getSalonMessage(@PathVariable("i") String c) {
        List<Message> listMessage = message.getListMessage(c);
        String result = "";
        for(int i = 0;i < listMessage.size(); i++){
            result = result + " " + listMessage.get(i).getTexte() + " ";
        }
        Map<String, Object> map = new HashMap<>();
        map.put("message", result);
        return new ModelAndView("SimpleString", map);
    }

    /**
     *
     * @param c le salon
     * @return les messages d'un salon
     */
    @RequestMapping(value="{i}" ,
            produces={"application/xml", "application/json"},
            method= RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    List<Message> getSalonMessageXMLJSON(@PathVariable("i") String c) {
        return message.getListMessage(c);
    }

    @RequestMapping(value="{i}/nbMessages")
    public ModelAndView getSalonMessageNb(@PathVariable("i") String c) {
        ArrayList<Message> listMessage = message.getListMessage(c);
        String result = " " + listMessage.size() + " ";
        Map<String, Object> map = new HashMap<>();
        map.put("message", result);
        return new ModelAndView("SimpleString", map);
    }

    /**
     *
     * @param c le salon
     * @return le nb de message dans un salon
     */
    @RequestMapping(value="{i}/nbMessages" ,
            produces={"application/xml", "application/json"},
            method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    Integer getSalonMessageNbXMLJSON(@PathVariable("i") String c) {
        ArrayList<Message> listMessage = message.getListMessage(c);
        return listMessage.size();
    }

    @RequestMapping(value="{i}/Messages/{idMess}")
    public ModelAndView getMessageListAfterM(@PathVariable("i") String c,@PathVariable("idMess") int id) {
        ArrayList<Message> listMessage = message.getListMessage(c);
        String result = "";
        for(int i = id;i < listMessage.size(); i++){
            result = result + " " + listMessage.get(i).getTexte() + " ";
        }
        Map<String, Object> map = new HashMap<>();
        map.put("message", result);
        return new ModelAndView("SimpleString", map);
    }

    /**
     *
     * @param c le salon
     * @param id le num du message
     * @return les messages d'un salon à partir d'un num de message
     */
    @RequestMapping(value="{i}/Messages/{idMess}" ,
            produces={"application/xml", "application/json"},
            method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    List<String> getSalonMessageNbXMLJSON(@PathVariable("i") String c,@PathVariable("idMess") int id) {
        ArrayList<Message> listMessage = message.getListMessage(c);
        List<String> result = new ArrayList<>();
        for(int i = id;i < listMessage.size(); i++){
            result.add(listMessage.get(i).getTexte());
        }
        return result;
    }

    @RequestMapping(value="{i}/addMessage/{message}")
    public ModelAndView addMessage(@PathVariable("i") String c,@PathVariable("message") String id) {
        Message m = new Message(message.getUserList().get(0).getPseudo(),id,message.nbMessageSalon(c));
        message.addMessage(c, m);
        String result =  "Ajout au salon " + c + " le message : " + id;
        Map<String, Object> map = new HashMap<>();
        map.put("message", result);
        return new ModelAndView("SimpleString", map);
    }

    @RequestMapping(value="{i}/addMessagePseudo/{pseudo}/{message}")
    public ModelAndView addMessagePseudo(@PathVariable("i") String c,@PathVariable("pseudo") String pseudo, @PathVariable("message") String id) {
        Message m = new Message(pseudo,id,message.nbMessageSalon(c));
        message.addMessage(c, m);
        String result =  "Ajout au salon " + c + " le message : " + id;
        Map<String, Object> map = new HashMap<>();
        map.put("message", result);
        return new ModelAndView("SimpleString", map);
    }

    /**
     *
     * @param c le salon
     * @param id le message
     * @return ajoute un message
     */
    @RequestMapping(value="{i}/addMessage/{message}" ,
            produces={"application/xml", "application/json"},
            method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    List<Message> addMessageXMLJSON(@PathVariable("i") String c,@PathVariable("message") String id) {
        Message m = new Message(message.getUserList().get(0).getPseudo(),id,message.nbMessageSalon(c));
        message.addMessage(c, m);
        return message.getListMessage(c);
    }

    @RequestMapping(value="{i}/deleteSalon")
    public ModelAndView deleteSalon(@PathVariable("i") String c) {
        message.getListMessage(c).clear();
        String result = "Le salon " + c + " a été supprimé.";
        Map<String, Object> map = new HashMap<>();
        map.put("message", result);
        return new ModelAndView("SimpleString", map);
    }

    /**
     *
     * @param c le salon
     * @return supprime un salon
     */
    @RequestMapping(value="{i}/deleteSalon" ,
            produces={"application/xml", "application/json"},
            method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    List<Message> deleteSalonXMLJSON(@PathVariable("i") String c) {
        message.getListMessage(c).clear();
        return message.getListMessage(c);
    }
}
