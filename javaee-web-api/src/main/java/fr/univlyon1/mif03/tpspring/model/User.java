package fr.univlyon1.mif03.tpspring.model;

import java.util.ArrayList;

public class User {
    private static int id = 0;
    private String pseudo;
    private ArrayList<String> salon;

    public User(String pseudo){
        this.pseudo = pseudo;
        id++;
    }

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        User.id = id;
    }

    public void addSalon(String s){
        int existe = 0;
        for(int i = 0;i < salon.size();i++){
            if(salon.get(i).equals(s)){
                existe = 1;
            }
        }
        if(existe == 0) {
            salon.add(s);
        }
    }

    public ArrayList<String> getSalon() {
        return salon;
    }

    public void setSalon(ArrayList<String> salon) {
        this.salon = salon;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }
}
