package fr.univlyon1.mif03.tpspring.restjersey;

import fr.univlyon1.mif03.tpspring.beans.GestionMessage;
import fr.univlyon1.mif03.tpspring.model.User;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.ws.rs.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@EnableWebMvc
@Path("/UserService")
public class UserService {
    GestionMessage message = new GestionMessage();

    //Page HTML si le client ne l'accepte pas
    @RequestMapping(value="getUser")
    public ModelAndView getUser() {
        List<User> u = getUserXMLJSON();
        String result = "";
        for(int i = 0; i < u.size(); i++){
            result += u.get(i).getPseudo() + " ";
        }
        Map<String, Object> map = new HashMap<>();
        map.put("message", result);
        return new ModelAndView("SimpleString", map);
    }

    //Page JSON ou XML sinon
    @RequestMapping(value = "getUser" ,
            produces = {"application/xml", "application/json"},
            method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    List<User> getUserXMLJSON() {
        return message.getUserList();
    }

    //Ne marche pas
    //L'ajout de salon pour un utilisateur n'est pas fait
    @Path("salon/{c}")
    @GET
    @Produces("application/xml")
    public String getSalonByPseudo(@PathParam("c") int c) {
        User u = message.getUserList().get(c);
        String result = "";
        for(int i = 0;i < u.getSalon().size(); i++){
            result = "<salon>" + u.getSalon().get(i) + "</salon>";
        }
        return "<UserService>" + result + "</UserService>";
    }

    @RequestMapping(value="changePseudo/{i}")
    public ModelAndView changePseudo(@PathVariable("i") String c) {
        User u = changePseudoXMLJSON(c);
        Map<String, Object> map = new HashMap<>();
        map.put("message", u.getPseudo());
        return new ModelAndView("SimpleString", map);
    }

    @RequestMapping(value="changePseudo/{i}" ,
            produces = {"application/xml", "application/json"},
            method=RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    User changePseudoXMLJSON(@PathVariable("i") String c) {
        User u = message.getUserList().get(0);
        u.setPseudo(c);
        return message.getUserList().get(0);
    }

}

