import fr.univlyon1.mif03.tpspring.beans.GestionMessage;
import fr.univlyon1.mif03.tpspring.model.User;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/Deco")
public class Deco extends HttpServlet {
    GestionMessage message = new GestionMessage();

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.removeAttribute("pseudo");
        response.sendRedirect("index.html");

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        message.delUser(request.getParameter("actual"));
    }

}
