<%@ page import="fr.univlyon1.mif03.tpspring.model.Message" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: Jenny
  Date: 01/11/2018
  Time: 16:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
</head>
<body>
<jsp:useBean id="message" class="fr.univlyon1.mif03.tpspring.beans.GestionMessage"/>

<%


    ArrayList<Message> listMessage = message.getListMessage(request.getSession().getAttribute("salon").toString());

    for (int i = 0; i < listMessage.size(); i++) {
        out.print("<p>" + listMessage.get(i).getPseudo() + " : " + listMessage.get(i).getTexte() + "</p>");
    }

    Cookie cookie = new Cookie("DernierMessage",String.valueOf(listMessage.size()));
    cookie.setMaxAge(60*60*24);
    for(int i = 0; i < request.getCookies().length;i++){
        request.getCookies()[i].setMaxAge(0);
    }
    response.addCookie(cookie);
%>
</body>
</html>
