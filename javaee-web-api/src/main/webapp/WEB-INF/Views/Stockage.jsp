<%@ page import="fr.univlyon1.mif03.tpspring.model.Message" %>
<%@ page import="java.util.Date" %><%--
  Created by IntelliJ IDEA.
  User: Jenny
  Date: 01/11/2018
  Time: 16:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<jsp:useBean id="message" class="fr.univlyon1.mif03.tpspring.beans.GestionMessage"/>
<%

    message.setSalon(request.getSession().getAttribute("salon").toString());
    if (request.getParameter("message") != null) {
        message.addMessage(new Message(request.getSession().getAttribute("pseudo").toString(), request.getParameter("message")));
    }
    message.setDerniereModif(new Date());

%>

<jsp:forward page="Messages.jsp"/>

</body>
</html>
