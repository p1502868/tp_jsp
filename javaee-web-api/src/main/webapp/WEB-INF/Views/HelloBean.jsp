<%@ page import="java.util.List" %>
<%@ page import="fr.univlyon1.mif03.tpspring.model.Message" %><%--
  Created by IntelliJ IDEA.
  User: Thomas
  Date: 05/11/2018
  Time: 18:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Hello Beans</title>
</head>
<body>
<%!
%>
    <%
        if (request.getAttribute("message") instanceof List) {
            List<Message> messages = (List<Message>) request.getAttribute("message");
            for (int i = 0; i < messages.size(); i++) {
                out.print("<p>" + messages.get(i).getPseudo() + " : " + messages.get(i).getTexte() + "</p>");
            }
            ;
        }
        else {
            Message message = (Message)request.getAttribute("message");
            out.print("<p>" + message.getPseudo() + " : " + message.getTexte() + "</p>");
        }
    %>
<a href="\index.html"><button value="retourAccueil">Retourner à l'accueil</button></a>
</body>
</html>
