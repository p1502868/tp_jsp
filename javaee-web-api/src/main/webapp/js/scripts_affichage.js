function cacherMenu() {
    if(document.getElementById("menu").hasAttribute("hidden")){
        document.getElementById("menu").removeAttribute("hidden");
        document.getElementById("menuIcon").innerHTML = "keyboard_arrow_down";
    }
    else {
        document.getElementById("menu").setAttribute("hidden", "true");
        document.getElementById("menuIcon").innerHTML = "keyboard_arrow_right";
    }
}


function afficherCacher(element) {
    if(document.getElementById(element).hasAttribute("hidden")) {
        if(element != "index" && readCookie("pseudo") == null) {
            document.getElementById("index").setAttribute("hidden", "true");
        document.getElementById("salons").setAttribute("hidden", "true");
        document.getElementById("users").setAttribute("hidden", "true");
        document.getElementById("salon").setAttribute("hidden", "true");
        document.getElementById("message").setAttribute("hidden", "true");
        document.getElementById("deco").setAttribute("hidden", "true"); 
        document.getElementById("needCo").removeAttribute("hidden");
        }
        else {
        document.getElementById("index").setAttribute("hidden", "true");
        document.getElementById("salons").setAttribute("hidden", "true");
        document.getElementById("users").setAttribute("hidden", "true");
        document.getElementById("salon").setAttribute("hidden", "true");
        document.getElementById("message").setAttribute("hidden", "true");
        document.getElementById("deco").setAttribute("hidden", "true");
        document.getElementById("needCo").setAttribute("hidden", "true");
        document.getElementById(element).removeAttribute("hidden");

        traiterElem(element);
        }
    }
}

function pageIndex() {
    if(readCookie("pseudo") != null){
        document.getElementById("index").innerHTML =
            "<h2>Bienvenue sur Chatons.org</h2>\n" +
            "<p>Vous êtes bien connecté(e) en tant que : " + readCookie("pseudo") + "</p>";
    } else {
        document.getElementById("index").innerHTML =
            "<h2>Bienvenue sur Chatons.org</h2>\n" +
            "                <form id=\"connexion\">\n" +
            "                    <p>Entrez votre pseudo :\n" +
            "                        <input type=\"text\" name=\"pseudo\" id=\"pseudo\">\n" +
            "                        <input type=\"submit\" value=\"Connexion\">\n" +
            "                    </p>\n" +
            "                </form>"
    };
}

function changeColor(n) {
    document.getElementsByClassName("collection-item-clicked")[0].setAttribute("class", "collection-items");
    n.childNodes[0].setAttribute("class", "collection-item-clicked");
}