var reload;
var myHeaders = new Headers();

$( document ).ready(function() {
    if(readCookie("pseudo")!=null) {
        $.ajax({
            url: "UserService/getUser",
        }).then(function(r){
            exist = false;
            r = r.split("<p>")[1].split("</p>")[0].split(" ");
            for(i = 0; i < r.length; i++) {
                if(r[i]==readCookie("pseudo")) exist = true;
            }
            if(!exist) {
                document.cookie = "pseudo=; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
                pageIndex();

            }
        });
    }});


$(document).on('submit', '#connexion', function(event) {
    var form = $(this);
    var url = "/userCreation";
    document.cookie = "pseudo=" + document.getElementById("pseudo").value;
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize()
    });

    pageIndex();
    
    event.preventDefault();
});

$(document).on('submit', '#deconnexion', function(event) {

    if($('#delUser').is(":checked")){
        var url = "/delUser";
        $.post("Deco", {actual:readCookie("pseudo")});

    }
    document.cookie = "pseudo=; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
    window.location.replace("#index");
    pageIndex();
    afficherCacher('index');

    event.preventDefault();
});

$(document).on('submit', '#createSalon', function(event) {
    var form = $(this);
    var url = "/SalonService/addSalon/" + document.getElementById("salonN").value;
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        beforeSend: function(req) {
            req.setRequestHeader('Accept', '')
        }

});

    $("#salonsList").append("<li><a onclick='goToSalon(this)' id=\"" + $("#salonN").val() + "\" class=\"collection-item\">" + $("#salonN").val() + "</a></li>");
    $("#salonN").val("");

    event.preventDefault();
});

$(document).on('submit', '#messageForm', function(event) {
    var form = $(this);
    var url = "/SalonService/" + readCookie("salon") + "/addMessagePseudo/"+ readCookie("pseudo") + "/" + $("#newMessage").val();
    $.ajax({
        type: "POST",
        url: url
    });
    $("#newMessage").val('');
    getMessages();

    event.preventDefault();
});

$(document).on('submit', '#detailMessage', function(event) {
    var url = "/MessageService/changeMessageS/"+readCookie("salon")+"/"+$("#msgText").attr('name')+"/"+$("#msgText").val();
    $.ajax({
        url: url,
        type : "POST"
    });
    window.location.replace("#salon");
    afficherCacher("salon");
    getMessages();
    event.preventDefault();
});

$(document).on('submit', '#deleteMessage', function(event) {
    var url = "/MessageService/deleteMessageS/"+readCookie("salon")+"/"+$("#msgText").attr('name');
    $.ajax({
        url: url,
        type : "POST"
    });
    window.location.replace("#salon");
    afficherCacher("salon");
    getMessages();
    event.preventDefault();
});


function traiterElem(element) {
    switch(element) {
        case "users":
            userList();
            break;

        case "salons":
            salonList();
            break;


        default:
            break;
    }
}

function userList() {
    $.ajax({
        url: "/UserService/getUser.json"
    }).then(function(json){
        $("#usersList").html("");
        for(i = 0; i < json.length; i ++){
            $("#usersList").append("<li>" + json[i].pseudo + "</li>");
        }
    });
}

function salonList() {
    $.ajax({
        url: "/SalonService/getSalon.json"
    }).then(function(json){
        $("#salonsList").html("");
        for(i = 0; i < json.length; i ++){
            $("#salonsList").append("<li><a onclick='goToSalon(this)' id=\"" + json[i] + "\" class=\"collection-item\">" + json[i] + "</a></li>");
        }
    });
}

function goToSalon(n) {
    document.cookie = "salon=" + n.id;
    window.location.replace("#salon");
    afficherCacher('salon');
    $("#actualSalon").html("Salon " + n.id);
    getMessages();
}

function getMessages() {
    var pseudoL = "";
    $.ajax({
        url: "/SalonService/" + readCookie("salon") + ".json"
    }).then(function(json){
        $("#msgList").html("");
        if(json.length >0) {
            $("#msgList").append("<ul id='mesgs'></ul>");
            for (i = 0; i < json.length; i++) {
                if (json[i].pseudo != null) {
                    if(json[i].pseudo != pseudoL) $("#mesgs").append("<div name=" + json[i].pseudo +"></div>");
                    $('div[name='+json[i].pseudo+']').last().append("<li><a class='messageLink' href='#message' onclick='afficherCacher(\"message\"); getMessageData(" + json[i].idMessage + ")'><u>" + json[i].pseudo + "</u>: " + json[i].texte + "</a></li>");

                }
                pseudoL = json[i].pseudo;
            }
        }
    });

    reload = setTimeout('getMessages()', 5000);
}

function getMessageData(id) {
    $.ajax({
        url: "/MessageService/messageInfo/"+readCookie("salon")+ "/"+(id)+".json"
    }).then(function(json){
        $("#msgAuthor").val(json.pseudo);
        $("#msgText").val(json.texte);
        $("#msgText").attr('name', id);
        $("#msgNumber").html("Message numéro " + id);
    });
}
